class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        if not l1:
            return l2
        elif not l2:
            return l1
        
        que = ListNode()
        que.next = None
        cur = que

        while l1 or l2:
            if l1 and l2:
                if l1.val <= l2.val:
                    cur.next = l1
                    l1 = l1.next
                else:
                    cur.next = l2
                    l2 = l2.next
                cur = cur.next
                continue

            if not l1:
                cur.next = l2
            else:
                cur.next = l1
            break
        
        return que.next