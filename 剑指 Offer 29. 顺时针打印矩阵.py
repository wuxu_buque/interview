from typing import List

class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        
        if not matrix:
            return []

        direct = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        rows = len(matrix)
        cols = len(matrix[0])
        visited = [[0 for j in range(cols)] for i in range(rows)]
        
        m, n = 0, -1
        index = 0
        step = 0
        res = []

        while step != rows*cols:
            (x, y) = direct[index % 4]
            m += x
            n += y
            if 0 <= m < rows and 0 <= n < cols and visited[m][n] == 0:
                visited[m][n] = 1
                res.append(matrix[m][n])
                step += 1
            else:
                # 换方向
                m -= x
                n -= y
                index += 1
        return res

if __name__ == "__main__":
    s = Solution()
    matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
    print(s.spiralOrder(matrix))

