from typing import List

class Solution:
    def printNumbers(self, n: int) -> List[int]:
        m = 9

        while True:
            n = n - 1
            if n == 0:
                break;

            m = m*10+9
        
        return [i for i in range(1, m + 1)]
            

