class Solution:
    def reverseWords(self, s: str) -> str:
        if not s:
            return ""
        
        l = s.strip().split()
        size = len(l)
        new_str = ""
        for i in range(size - 1, -1, -1):

            l[i] = l[i].replace(" ", "")
            if i == 0:
                new_str = "".join([new_str, l[i]])
            else:
                new_str = "".join([new_str, l[i], " "])
        
        return new_str
        
if __name__ == "__main__":
    s = Solution()
    print(s.reverseWords("a good   example"))
    