# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def getKthFromEnd(self, head: ListNode, k: int) -> ListNode:
        cur = head
        for i in range(1, k):
            if cur:
                cur = cur.next
            else:
                return None
        pre = head
        while cur.next:
            cur = cur.next
            pre = pre.next
        
        return pre
        
