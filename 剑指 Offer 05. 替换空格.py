class Solution:
    def replaceSpace(self, s: str) -> str:
        res = []
        for ch in s:
            if ch != " ":
                res.append(ch)
            else:
                res.append("%20")
        return "".join(res)