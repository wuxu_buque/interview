from typing import List
from collections import Counter

class Solution:
    def isStraight(self, nums: List[int]) -> bool:
        #排序
        nums.sort()
        cnt = Counter(nums)
        res = cnt.most_common(1)
        king = 0
        
        # 存在重复的非0
        if res[0][1] > 1 and res[0][0] != 0:
            return False
        if res[0][0] == 0:
            king = res[0][1] 
        

        for i in range(king):
            nums.remove(0)

        for i in range(len(nums) - 1):
            j = i + 1
            if nums[j] - nums[i] == 1:
                continue
            if nums[j] - nums[i] == 0:
                return False

            if nums[j] - nums[i] != 1 and king > 0:
                king = king - (nums[j] - nums[i]) + 1
                if king < 0:
                    return False
                continue
            else:
                return False
        
        return True

if __name__ == "__main__":
    nums = [2, 2, 0, 0, 5]
    s = Solution()
    print(s.isStraight(nums))

        
