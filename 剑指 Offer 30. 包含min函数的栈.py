from typing import List

class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = []

        # 存储的数据结构为(value, min)

    def push(self, x: int) -> None:
        if not self.stack:
            min_value = x
        else:
            min_value = self.stack[-1][1]
            min_value = min(x, min_value)
        
        self.stack.append((x, min_value))

    def pop(self) -> None:
        if self.stack:
            self.stack.pop()

    def top(self) -> int:
        if self.stack:
            return self.stack[-1][0]
        else:
            return -1

    def min(self) -> int:
        if self.stack:
            return self.stack[-1][1]
        else:
            return -1

if __name__ == "__main__":
    s = MinStack()
    s.push(-2)
    s.push(0)
    s.push(-3)
    print(s.min())
    print(s.pop())
    print(s.top())
    print(s.min())