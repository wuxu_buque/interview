from typing import List

class Solution:
    def findRepeatNumber(self, nums: List[int]) -> int:
        maps = set()

        for i in nums:
            if i not in maps:
                maps.add(i)
            else:
                return i

if __name__ == "__main__":
    nums = [2, 3, 1, 0, 2, 5, 3]
    s = Solution()

    print(s.findRepeatNumber(nums))