# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None
class Solution:
    def deleteNode(self, head: ListNode, val: int) -> ListNode:
        head1 = ListNode()
        head1.next = head

        if not head:
            return None
        
        pre = head1
        cur = head1.next
        while cur:
            if cur.val == val:
                pre.next = cur.next
            cur = cur.next
        return head1.next