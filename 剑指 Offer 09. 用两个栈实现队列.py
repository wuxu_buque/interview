from typing import List

class CQueue:

    def __init__(self):
        self.q1 = []
        self.q2 = []

    def appendTail(self, value: int) -> None:
        self.q1.append(value)


    def deleteHead(self) -> int:
        while self.q1:
            self.q2.append(self.q1.pop())

        if self.q2:
            value = self.q2.pop()
        else:
            return -1

        while self.q2:
            self.q1.append(self.q2.pop())
        
        return value

if __name__ == "__main__":
    # Your CQueue object will be instantiated and called as such:
    obj = CQueue()
    obj.appendTail(1)
    obj.appendTail(2)
    obj.appendTail(3)
    print(obj.deleteHead())
    print(obj.deleteHead())
    print(obj.deleteHead())
    print(obj.deleteHead())