# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        pre = None
        cur = None
        pro = None

        if not head:
            return None

        pre = head
        if pre.next:
            cur = pre.next
            pre.next = None
            if cur.next:
                pro = cur.next
        else:
            return head

        while cur:
            cur.next = pre
            pre = cur
            cur = pro
            if pro:
                pro = pro.next
            
        return pre