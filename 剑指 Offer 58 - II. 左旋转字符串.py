class Solution:
    def reverseLeftWords(self, s: str, n: int) -> str:
        if not s:
            return None

        return "".join([s[n:], s[:n]])


if __name__ == "__main__":
    s = Solution()
    print(s.reverseLeftWords("abc", 2))