from collections import Counter

class Solution:
    def firstUniqChar(self, s: str) -> str:
        count = Counter(s)
        for i in s:
            if count[i] == 1:
                return i
        return " "


if __name__ == "__main__":
    s = Solution()
    s1 = "aabb"
    print(s.firstUniqChar(s1))
