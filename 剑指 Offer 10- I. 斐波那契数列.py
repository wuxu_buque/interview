
# F(0) = 0,   F(1) = 1
# F(N) = F(N - 1) + F(N - 2), 其中 N > 1.

class Solution:
    def fib(self, n: int) -> int:
        x = 1000000007
        if n <= 1:
            return n

        for i in range(0, n + 1):
            if i == 0:
                cur = 0
                pre = 0
                continue
            elif i == 1:
                cur = 1
                continue

            tmp = (cur + pre) % x
            pre = cur
            cur = tmp

        return cur

if __name__ == "__main__":
    s = Solution()
    n = 45
    print(s.fib(n))

