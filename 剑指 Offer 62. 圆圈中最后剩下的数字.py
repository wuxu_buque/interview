class Solution:
    def lastRemaining(self, n: int, m: int) -> int:
        self.lastRemaining(n - 1, m%(n-1))

if __name__ == "__main__":
    s = Solution()
    print(s.lastRemaining(5, 3))
