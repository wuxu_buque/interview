from typing import List

class Solution:
    def QuickSort(self, nums: List[int]) -> List[int]:
        size = len(nums)
        if size <= 1:
            return nums
        
        left = []
        right = []
        key = nums[-1]
        nums.remove(key)
        for num in nums:
            if num <= key:
                left.append(num)
            else:
                right.append(num)

        return self.QuickSort(left) + [key] + self.QuickSort(right)


if __name__ == "__main__":
    nums = [3, 4, 1, 8, 10, 2]
    s = Solution()
    print(s.QuickSort(nums))
    