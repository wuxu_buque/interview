# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

from typing import List
from collections import deque

class Solution:
    def levelOrder(self, root: TreeNode) -> List[int]:
        if not root:
            return []
        res = []
        que = deque()
        que.append(root)

        while que:
            node = que.popleft()
            res.append(node.val)
            if node.left:
                que.append(node.left)
            if node.right:
                que.append(node.right)

        return res
