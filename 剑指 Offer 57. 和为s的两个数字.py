from typing import List

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        size = len(nums)

        for i in range(size - 1, 0, -1):
            if nums[i] >= target:
                continue
            for j in range(0, i):
                if i == j:
                    continue

                if nums[i] + nums[j] > target:
                    break

                if nums[i] + nums[j] == target:
                    return [nums[i], nums[j]]

        return []


if __name__ == "__main__":
    nums = [10,26,30,31,47,60]
    target = 4
    s = Solution()
    print(s.twoSum(nums, target))
