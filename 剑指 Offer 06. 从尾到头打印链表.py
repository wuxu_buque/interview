from typing import List

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

# 1->2->3
class Solution:
    def reversePrint(self, head: ListNode) -> List[int]:
        res = []
        pre = None
        cur = None
        pro = None

        if not head:
            return res
        
        pre = head

        if head.next:
            cur = head.next
            if cur.next:
                pro = cur.next
            pre.next = None
        else:
            return [head.val]

        while cur:
            cur.next = pre
            pre = cur
            cur = pro
            if pro:
                pro = pro.next
            else:
                break
        
        while pre:
            res.append(pre.val)
            pre = pre.next

        return res
